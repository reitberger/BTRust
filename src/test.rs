#[cfg(test)]
#[macro_export]
macro_rules! insert_tree{
	($x:ident, $($i:expr),*$(,)?) => {
		$($x.insert($i);)*
	};
}
mod test {
    use crate::{avl::AVLTree, ord::BTree};
    //#[test]
    //fn t0() {
    //    let mut a = BTree::new(0);
    //    insert_tree!(a, 1, 2);
    //    a.remove(2);
    //    print!("{:?}", a);
    //    panic!();
    //}
    //#[test]
    //fn t2() {
    //    let mut tree = BTree::new(10);
    //    insert_tree!(tree, 5, 6);
    //    tree.remove(5);
    //    print!("{:?}", tree);
    //    panic!();
    //}
    //#[test]
    //fn t1() {
    //    let mut tree = BTree::new(10);
    //    insert_tree!(tree, 5, 3, 1, 2, 4, 6, 8, 7, 9, 15, 13, 12, 14, 17, 16, 18,);
    //    tree.remove(5);
    //    print!("{:?}", tree);
    //    panic!();
    //}

    #[test]
    fn avl_rotate_0() {
        let mut tree = AVLTree::new(5);
        insert_tree!(tree, 1, 10, 6);
        print!("{:?}\n", tree);
        tree.rotate_right();
        print!("{:?}", tree);
        panic!();
    }
    #[test]
    fn avl_rotate_video0() {}
    //    #[test]
    //    fn t0() {
    //        let mut a = BTree::new();
    //        a.insert(2);
    //        a.insert(2);
    //        a.insert(2);
    //        print!("{:?}\n", a);
    //        a.print();
    //        panic!();
    //    }
}
