use std::cmp::{Ordering, PartialOrd};
type NodePtr<T> = Option<Box<Node<T>>>;
#[derive(Debug)]
pub struct Node<T: PartialOrd> {
    value: T,
    left: NodePtr<T>,
    right: NodePtr<T>,
}
impl<T: PartialOrd> Node<T> {
    pub fn new(value: T) -> Self {
        Self {
            value,
            left: None,
            right: None,
        }
    }
}
impl<T: PartialOrd> Into<NodePtr<T>> for Node<T> {
    fn into(self) -> NodePtr<T> {
        Some(Box::new(self))
    }
}
#[derive(Debug)]
pub struct BTree<T: PartialOrd> {
    root: NodePtr<T>,
}

impl<T: PartialOrd> BTree<T> {
    pub fn new(root: T) -> Self {
        Self {
            root: Node::new(root).into(),
        }
    }
    pub fn insert(&mut self, value: T) {
        Self::insert_children(&mut self.root, value);
    }
    fn insert_children(ptr: &mut NodePtr<T>, value: T) {
        match ptr {
            None => *ptr = Node::new(value).into(),
            Some(ptr) => {
                if value > ptr.value {
                    Self::insert_children(&mut ptr.right, value)
                } else {
                    Self::insert_children(&mut ptr.left, value)
                }
            }
        }
    }
    pub fn remove(&mut self, value: T) -> Option<T> {
        Self::remove_children(&mut self.root, value)
    }
    fn remove_children(ptra: &mut NodePtr<T>, value: T) -> Option<T> {
        if let Some(ptr) = ptra {
            match ptr.value.partial_cmp(&value) {
                Some(ordering) => match ordering {
                    Ordering::Equal => {
                        let right_tree = ptr.right.take();
                        let left_tree = ptr.left.take();
                        *ptra = left_tree;
                        match ptra {
                            None => {
                                *ptra = right_tree;
                                Some(value)
                            }
                            Some(ptr) => {
                                let mut cur_right = &mut ptr.right;
                                while let Some(right) = cur_right {
                                    cur_right = &mut right.right;
                                }
                                *cur_right = right_tree;
                                Some(value)
                            }
                        }
                    }
                    Ordering::Less => Self::remove_children(&mut ptr.right, value),
                    Ordering::Greater => Self::remove_children(&mut ptr.left, value),
                },
                _ => None,
            }
        } else {
            None
        }
    }
}
