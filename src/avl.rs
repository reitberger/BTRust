use std::{
    cell::RefCell,
    cmp::Ordering,
    rc::{Rc, Weak},
};

type NodePtr<T> = Option<Rc<RefCell<Node<T>>>>;
type WeakNodePtr<T> = Option<Weak<RefCell<Node<T>>>>;
#[derive(Debug)]
pub struct Node<T> {
    value: T,
    left: NodePtr<T>,
    right: NodePtr<T>,
    parent: WeakNodePtr<T>,
}
impl<T> Node<T> {
    pub fn new(value: T) -> Self {
        Self {
            value,
            left: None,
            right: None,
            parent: None,
        }
    }
    pub fn parent(self, parent: WeakNodePtr<T>) -> Self {
        Self {
            value: self.value,
            left: self.left,
            right: self.right,
            parent,
        }
    }
}
impl<T> Into<NodePtr<T>> for Node<T> {
    fn into(self) -> NodePtr<T> {
        Some(Rc::new(RefCell::new(self)))
    }
}
#[derive(Debug)]
pub struct AVLTree<T> {
    root: NodePtr<T>,
}
impl<T: std::cmp::PartialOrd> AVLTree<T> {
    pub fn new(value: T) -> Self {
        Self {
            root: Node::new(value).into(),
        }
    }
    pub fn insert(&mut self, value: T) {
        Self::insert_children(&mut self.root, None, value)
    }
    fn insert_children(ptr: &mut NodePtr<T>, parent_ptr: WeakNodePtr<T>, value: T) {
        match ptr {
            None => *ptr = Node::new(value).parent(parent_ptr).into(),
            Some(ref ptr) => {
                if ptr.borrow().value > value {
                    Self::insert_children(
                        &mut ptr.borrow_mut().left,
                        Some(Rc::downgrade(ptr)),
                        value,
                    );
                } else {
                    Self::insert_children(
                        &mut ptr.borrow_mut().right,
                        Some(Rc::downgrade(ptr)),
                        value,
                    );
                }
            }
        }
    }
    pub fn remove(&mut self, value: T) -> Option<()> {
        Self::remove_children(&mut self.root, value)
    }
    fn remove_children(ptr: &mut NodePtr<T>, value: T) -> Option<()> {
        let result = ptr.as_mut()?.borrow().value.partial_cmp(&value).unwrap();
        match result {
            Ordering::Equal => {
                *ptr = None;
                return Some(());
            }
            Ordering::Less => Self::remove_children(&mut ptr.as_mut()?.borrow_mut().left, value),
            Ordering::Greater => {
                Self::remove_children(&mut ptr.as_mut()?.borrow_mut().right, value)
            }
        }
    }
    pub fn rotate_left(&mut self) -> Option<()> {
        Self::rotate_left_node(&mut self.root)
    }
    pub fn rotate_left_node(ptr: &mut NodePtr<T>) -> Option<()> {
        todo!();
    }
    pub fn rotate_right(&mut self) -> Option<()> {
        Self::rotate_right_node(&mut self.root)
    }
    pub fn rotate_right_node(ptr: &mut NodePtr<T>) -> Option<()> {
        if let Some(_) = ptr {
            let mut root = ptr.as_mut().unwrap().borrow_mut();
            // new root
            let left = root.left.take();
            let left_right = match left {
                None => None,
                Some(ref left) => left.borrow_mut().right.take(),
            };
            drop(root);
            let prev_root = ptr.take();
            // NOTE: parent can be none!
            // TODO: Check for possible allowed none ptrs and clean this up

            left.as_ref().unwrap().borrow_mut().parent =
                prev_root.as_ref()?.borrow_mut().parent.clone();
            prev_root.as_ref().unwrap().borrow_mut().parent = match left {
                None => None,
                Some(ref left) => Some(Rc::downgrade(left)),
            };
            if let Some(ref left_right) = left_right {
                left_right.borrow_mut().parent = match prev_root {
                    None => None,
                    Some(ref prev_root) => Some(Rc::downgrade(prev_root)),
                };
            }

            prev_root.as_ref().unwrap().borrow_mut().left = left_right;
            //left_parent.borrow_mut() = Rc::clone(prev_root_parent);
            *ptr = left;
            ptr.as_ref().unwrap().borrow_mut().right = prev_root;
        }
        Some(())
    }
}
